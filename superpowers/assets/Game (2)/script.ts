let actorTom = new Sup.Actor("Padawan");
new Sup.SpriteRenderer(actorTom, "Tom");
let actorVince = new Sup.Actor("Encyclopedia");
new Sup.SpriteRenderer(actorVince, "Vince");
let actorDottie = new Sup.Actor("Dog");
new Sup.SpriteRenderer(actorDottie, "Dottie");

let actorRob = new Sup.Actor("Rob");
new Sup.Camera(actorRob);

actorTom.setPosition(1, 0, 0);
actorVince.setPosition(-3, 0, 0);
actorDottie.setPosition(-1, 6, 0);

actorRob.setPosition(-2, 1, 20);